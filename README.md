# nmdc-telegramfrontend

![](https://img.shields.io/badge/written%20in-Go-blue.svg)

A bot to synchronise chat between an DC hub and a Telegram supergroup.

## Features

- Supports NMDC / NMDCS / ADC / ADCS hubs
- Automatically upload photos/files/videos/audio/stickers to a Contented server for DC users
- Fallback upload to thumbnail-only if exceeding declared Contented file size limit
- Convert telegram quoted messages to DC quoting style
- Exclude messages from multiple hub nicks (e.g. `Hub-Security` / `PtokaX` and helper bots)
- Standalone binary
- PM with native users
- Get native userlist inside the telegram group chat
- Option for length requirement on native nick
- Disconnect telegram users who fail to log in to the upstream hub

## Setup

Create a new telegram bot

1. Use BotFather to create a new bot
2. Use BotFather to disable its privacy mode for group chats
3. Use BotFather to add commands (that appear in the groupchat). Recommendation: `userlist - List native online users`
	
Create a telegram group

1. Manually create a group chat and add the bot to it
2. Convert group chat to supergroup
3. Grant bot to be an administrator (including ability to add more administrators)
4. Settings > "Who can add members" > Only administrators
5. Create an invite link
	
Handover to `nmdc-telegramfrontend`

1. Run this bot with no `-GroupChatID`, to learn the groupchat ID
2. Post a test message in the group chat, to discover the groupchat ID
3. Leave the group chat (long press on mobile, can't do it on desktop)
4. Run this bot with `-GroupChatID` for normal operation

## Usage

Chat with the bot to enter/leave the synchronised channel.

Sometimes the telegram invite links can take a few minutes to activate, especially if there has been unusual activity (e.g. frequent join/parts)

## Changelog

2018-06-10 1.0.1
- Send DC captions on TG photos and voice clips
- Send DC message when sharing a TG contact
- Fix a cosmetic issue with missing format specifier in log message
- [⬇️ nmdc-telegramfrontend-1.0.1-src.tar.bz2](https://git.ivysaur.me/attachments/857a8a57-3dae-44b4-9250-8c3691b393e1) *(100.84 KiB)*
- [⬇️ nmdc-telegramfrontend-1.0.1-linux64.tar.xz](https://git.ivysaur.me/attachments/86a10b0f-0ca5-4150-820c-e27451198a27) *(1.92 MiB)*

2018-06-09 1.0.0
- Initial public release
- [⬇️ nmdc-telegramfrontend-1.0.0-src.tar.bz2](https://git.ivysaur.me/attachments/0d357ce3-b026-45d0-b722-8f4f0cd47be5) *(101.17 KiB)*
- [⬇️ nmdc-telegramfrontend-1.0.0-linux64.tar.xz](https://git.ivysaur.me/attachments/643ad20b-814a-4883-94fa-876a4c361a83) *(1.92 MiB)*

2018-06-03
- Private beta
