package main

import (
	"flag"
	"log"
)

func main() {

	configFile := flag.String("ConfigFile", "config.json", "")
	verbose := flag.Bool("Verbose", false, "")
	flag.Parse()

	svr, err := NewNTFServer(*configFile, *verbose)
	if err != nil {
		log.Fatal(err.Error())
	}

	err = svr.Run()
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Println("Server shutting down")
}
