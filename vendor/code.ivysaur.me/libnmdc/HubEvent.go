package libnmdc

type HubEventType int

const (
	EVENT_PUBLIC                   HubEventType = 1
	EVENT_PRIVATE                  HubEventType = 2
	EVENT_SYSTEM_MESSAGE_FROM_HUB  HubEventType = 3
	EVENT_SYSTEM_MESSAGE_FROM_CONN HubEventType = 4
	EVENT_USER_JOINED              HubEventType = 5
	EVENT_USER_PART                HubEventType = 6
	EVENT_USER_UPDATED_INFO        HubEventType = 7
	EVENT_CONNECTION_STATE_CHANGED HubEventType = 8
	EVENT_HUBNAME_CHANGED          HubEventType = 9
	EVENT_DEBUG_MESSAGE            HubEventType = 10
	EVENT_USERCOMMAND              HubEventType = 11
	EVENT_BAD_LOGIN_FAILURE        HubEventType = 12
)

type HubEvent struct {
	EventType   HubEventType
	Nick        string
	Message     string
	StateChange ConnectionState
	UserCommand *UserCommand
}
