package libnmdc

import (
	"net"
)

type ConnectionState int

const (
	CONNECTIONSTATE_DISCONNECTED = 1
	CONNECTIONSTATE_CONNECTING   = 2 // Handshake in progress
	CONNECTIONSTATE_CONNECTED    = 3
)

func (cs ConnectionState) String() string {
	switch cs {
	case CONNECTIONSTATE_DISCONNECTED:
		return "Disconnected"
	case CONNECTIONSTATE_CONNECTING:
		return "Connecting"
	case CONNECTIONSTATE_CONNECTED:
		return "Connected"
	default:
		return "?"
	}
}

func checkIsNetTimeout(err error) bool {
	if err == nil {
		return false
	}

	switch err.(type) {
	case net.Error:
		return err.(net.Error).Timeout()

	default:
		return false
	}
}
