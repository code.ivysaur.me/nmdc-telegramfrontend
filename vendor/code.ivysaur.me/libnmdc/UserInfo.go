package libnmdc

// This structure represents a user connected to a hub.
type UserInfo struct {
	Nick             string
	Description      string
	ClientTag        string
	ClientVersion    string
	Email            string
	ShareSize        uint64
	Flag             UserFlag
	Slots            uint64
	HubsUnregistered uint64
	HubsRegistered   uint64
	HubsOperator     uint64
	IsOperator       bool

	UserInfo_NMDCOnly
	UserInfo_ADCOnly
}

type UserInfo_NMDCOnly struct {
	Speed          string
	IPAddress      string
	ConnectionMode ConnectionMode
}

type UserInfo_ADCOnly struct {
	SharedFiles      uint64
	UploadSpeedBps   uint64
	DownloadSpeedBps uint64
	IsBot            bool
	IsRegistered     bool
	IsSuperUser      bool
	IsHubOwner       bool
	IPv4Address      string // Passive <==> these fields are not set
	IPv6Address      string
	IPv4UDPPort      uint64
	IPv6UDPPort      uint64
	Keyprint         string
	CID              string
	SupportFlags     map[string]struct{}
}

func NewUserInfo(username string) *UserInfo {
	return &UserInfo{
		Nick:             username,
		HubsUnregistered: 1,
		UserInfo_NMDCOnly: UserInfo_NMDCOnly{
			ConnectionMode: CONNECTIONMODE_PASSIVE,
		},
	}
}
