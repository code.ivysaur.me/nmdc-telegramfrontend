package libnmdc

type Protocol interface {
	ProcessCommand(msg string)

	SayPublic(string) error

	SayPrivate(user, message string) error

	SayInfo() error

	ProtoMessageSeparator() string
}

type HubProtocol int

const (
	HubProtocolAutodetect HubProtocol = 0
	HubProtocolNmdc       HubProtocol = 1
	HubProtocolAdc        HubProtocol = 2
)

func (hp HubProtocol) Create(hc *HubConnection) Protocol {
	if hp == HubProtocolNmdc {
		return NewNmdcProtocol(hc)

	} else if hp == HubProtocolAdc {
		return NewAdcProtocol(hc)

	} else {
		return NewAutodetectProtocol(hc)
	}
}
