module code.ivysaur.me/nmdc-telegramfrontend

require (
	code.ivysaur.me/libnmdc v0.0.0-20180604072808-74c20aaed6b2
	github.com/cxmcc/tiger v0.0.0-20170524142333-bde35e2713d7
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.2+incompatible
	github.com/technoweenie/multipartstreamer v1.0.1
	gopkg.in/h2non/filetype.v1 v1.0.5
)
