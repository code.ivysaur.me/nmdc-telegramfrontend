 # Makefile for nmdc-telegramfrontend
 
VERSION := 1.0.2
 
.PHONY: all dist clean

all: nmdc-telegramfrontend

clean:
	rm -f ./nmdc-telegramfrontend

nmdc-telegramfrontend: *.go Gopkg.*
	GOOS=linux GOARCH=amd64 go build -ldflags '-s -w'

dist: dist/nmdc-telegramfrontend-$(VERSION)-linux64.tar.xz dist/nmdc-telegramfrontend-$(VERSION)-src.tar.bz2

dist/nmdc-telegramfrontend-$(VERSION)-linux64.tar.xz: nmdc-telegramfrontend
	XZ_OPTS=-9 tar caf dist/nmdc-telegramfrontend-$(VERSION)-linux64.tar.xz nmdc-telegramfrontend --owner=0 --group=0
	
dist/nmdc-telegramfrontend-$(VERSION)-src.tar.bz2: nmdc-telegramfrontend
	hg archive dist/nmdc-telegramfrontend-$(VERSION)-src.tar.bz2
