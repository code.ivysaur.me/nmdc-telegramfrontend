package main

import (
	"encoding/json"
	"io/ioutil"
)

// NTFConfig keeps track of setup properties as well as runtime state.
// FIXME separate compile-time and run-time properties into separate files
type NTFConfig struct {
	HubAddr         string
	HubDescription  string
	HubIgnoreNicks  []string // Nicknames of Hub-Security/bots to exclude (e.g. "PtokaX").
	HubNickMinChars int
	ContentedURL    string
	ContentedMaxMB  int
	BotAPIKey       string
	GroupChatID     int64

	// Map of telegram user IDs to NMDC nicks
	KnownUsers map[int64]string

	// Map of telegram users known to be in the group chat ==> telegram displayname
	GroupChatMembers map[int64]string

	// Map of telegram users to their direct conversation ID with the bot
	DirectMessageChats map[int64]int64
}

func LoadConfig(configFile string) (NTFConfig, error) {
	b, err := ioutil.ReadFile(configFile)
	if err != nil {
		return NTFConfig{}, err
	}

	ret := NTFConfig{}
	err = json.Unmarshal(b, &ret)
	if err != nil {
		return NTFConfig{}, err
	}

	if ret.KnownUsers == nil {
		ret.KnownUsers = make(map[int64]string)
	}
	if ret.GroupChatMembers == nil {
		ret.GroupChatMembers = make(map[int64]string)
	}
	if ret.DirectMessageChats == nil {
		ret.DirectMessageChats = make(map[int64]int64)
	}

	return ret, nil
}

func (this *NTFConfig) Save(configFile string) error {
	b, err := json.MarshalIndent(this, "", "  ")
	if err != nil {
		return err
	}

	return ioutil.WriteFile(configFile, b, 0644)
}
